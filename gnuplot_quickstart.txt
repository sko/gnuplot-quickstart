Ce document propose une très très courte introduction à l'utilisation de gnuplot.

Gnuplot est un logiciel très simple qui permet de tracer des graphes à partir d'expression analytique ou aussi
d'ensembles de données.

1) ouvrir un terminal

2) vérifier que gnuplot est disponible dans l'environnement, dans le terminal : 
 $ which gnuplot
devrait retourner le chemin vers l'exécutable

3) lancer gnuplot dans le terminal:
 $ gnuplot

	G N U P L O T
	Version 4.4 patchlevel 3
	last modified March 2011
	System: Linux 2.6.38-11-generic

	Copyright (C) 1986-1993, 1998, 2004, 2007-2010
	Thomas Williams, Colin Kelley and many others

	gnuplot home:     http://www.gnuplot.info
	faq, bugs, etc:   type "help seeking-assistance"
	immediate help:   type "help"
	plot window:      hit 'h'

Terminal type set to 'x11'
gnuplot> 

4) on arrive dans le terminal de gnuplot

5) vérifier  que le terminal de sortie est x11, tapez dans le terminal de gnuplot:
gnuplot> show term
   terminal type is x11 

Si la réponse est différente, tapez:

gnuplot> set term x11


6) Affichage à l'écran de la courbe x |--> cos(x)
gnuplot> plot cos(x)

7) Affichage à l’écran de la courbe x ∈ [−2, 1.6] |--> x cos(x) avec des lignes
gnuplot> plot [ -2.0:1.6] cos ( x ) with lines

8) Ecriture dans un fichier png d'une courbe (il est possible que cela ne fonctionne pas si le terminal png n'est pas disponible)

gnuplot > set terminal png
gnuplot > set output "courbe.png"
gnuplot > plot [ -2.0:1.6] cos (x) with lines
gnuplot > set terminal x11

9) Affichage à l’écran de la surface de la fonction (x,y) ∈ [−1, 1.3][−2, 1] |--> sin(2 ∗ x ∗ y )
gnuplot > splot [ -1:1.3][ -2:1] sin (2*x*y )


10) Graphe d'un ensemble de points x_k, y_k, k=1,...,N définis dans le fichier example_XY.dat
gnuplot > plot 'example_XY.dat' with lines

dans le fichier chaque ligne contient un couple de valeur x_k et y_k séparés par un espace


11) Graphe d'un ensemble de points x_i, y_j, z_{i,j}, i=1,...,nx, j=1,...,ny 
définis dans le fichier example_XYZ.dat
gnuplot > splot 'example_XYZ.dat' with lines

le fichier est structuré en blocs
x_1 y_1 z_{1,1}
...
x_{nx} y_1 z_{nx,1}
<newline>
x_1 y_2 z_{1,2}
...
x_{nx} y_2 z_{nx,2}
<newline>
...
...
x_1 y_{ny} z_{1,ny}
...
x_{nx} y_{ny} z_{nx,ny}


